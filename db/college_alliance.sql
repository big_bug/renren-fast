/*
SQLyog Professional v12.08 (64 bit)
MySQL - 8.0.12 : Database - college_alliance
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`college_alliance` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `college_alliance`;

/*Table structure for table `t_banner` */

DROP TABLE IF EXISTS `t_banner`;

CREATE TABLE `t_banner` (
  `id` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL COMMENT '类型',
  `jumpid` varchar(100) DEFAULT NULL COMMENT '跳转id',
  `image_path` varchar(2000) DEFAULT NULL COMMENT '图片路径',
  `end_time` datetime DEFAULT NULL COMMENT '截止时间',
  `jumpmodel` varchar(100) DEFAULT NULL COMMENT '跳转类型',
  `deleted` tinyint(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `t_banner` */

/*Table structure for table `t_category` */

DROP TABLE IF EXISTS `t_category`;

CREATE TABLE `t_category` (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类别名称',
  `image_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类别图片',
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类别标题',
  `parent_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '父类别',
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  `wxapp_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '应用id',
  `status` int(10) DEFAULT NULL COMMENT '状态值 0为下线 1为正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `is_hot` int(2) DEFAULT NULL COMMENT '是否热门',
  `deleted` int(10) DEFAULT NULL COMMENT '逻辑删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_category` */

/*Table structure for table `t_comment` */

DROP TABLE IF EXISTS `t_comment`;

CREATE TABLE `t_comment` (
  `id` varchar(100) DEFAULT NULL,
  `accept_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '评价接受者id',
  `user_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '评价人员id',
  `content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '评价内容',
  `star_num` tinyint(2) DEFAULT NULL COMMENT '星级',
  `images` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '评价图片',
  `order_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_comment` */

/*Table structure for table `t_comment_reply` */

DROP TABLE IF EXISTS `t_comment_reply`;

CREATE TABLE `t_comment_reply` (
  `id` varchar(100) DEFAULT NULL,
  `comment_id` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '评论id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `user_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '回复内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_comment_reply` */

/*Table structure for table `t_job` */

DROP TABLE IF EXISTS `t_job`;

CREATE TABLE `t_job` (
  `id` varchar(100) DEFAULT NULL,
  `category_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类别id',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '内容',
  `weChat_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信号',
  `iphone` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `image_ids` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片集合',
  `contact_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系姓名',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态值',
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_job` */

/*Table structure for table `t_league` */

DROP TABLE IF EXISTS `t_league`;

CREATE TABLE `t_league` (
  `id` varchar(100) DEFAULT NULL,
  `category_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类别id',
  `title` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'title',
  `established` datetime DEFAULT NULL COMMENT '成立时间',
  `number_of_people` int(10) DEFAULT NULL COMMENT '人数',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '内容',
  `index_image_id` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '索引图片',
  `image_ids` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '介绍图片'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_league` */

/*Table structure for table `t_order` */

DROP TABLE IF EXISTS `t_order`;

CREATE TABLE `t_order` (
  `id` varchar(100) DEFAULT NULL,
  `order_no` varchar(100) DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  `pay_price` decimal(10,2) DEFAULT NULL,
  `pay_status` tinyint(2) DEFAULT NULL,
  `pay_time` datetime DEFAULT NULL,
  `express_price` decimal(10,2) DEFAULT NULL,
  `express_company` varchar(100) DEFAULT NULL,
  `express_no` varchar(100) DEFAULT NULL,
  `delivery_status` tinyint(2) DEFAULT NULL,
  `delivery_time` datetime DEFAULT NULL,
  `receipt_status` tinyint(2) DEFAULT NULL,
  `receipt_time` datetime DEFAULT NULL,
  `order_status` tinyint(2) DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `wxapp_id` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `deleted` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_order` */

/*Table structure for table `t_order_package` */

DROP TABLE IF EXISTS `t_order_package`;

CREATE TABLE `t_order_package` (
  `id` varchar(100) DEFAULT NULL,
  `package_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `deduct_stock_type` tinyint(2) DEFAULT NULL,
  `spec_type` tinyint(2) DEFAULT NULL,
  `spec_sku_id` varchar(100) DEFAULT NULL,
  `package_spec_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `package_attr` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `package_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `package_price` decimal(10,2) DEFAULT NULL,
  `line_price` decimal(10,2) DEFAULT NULL,
  `package_weight` decimal(10,2) DEFAULT NULL,
  `total_num` decimal(10,2) DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  `order_id` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `wxapp_id` varchar(100) DEFAULT NULL,
  `create_time` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_order_package` */

/*Table structure for table `t_package` */

DROP TABLE IF EXISTS `t_package`;

CREATE TABLE `t_package` (
  `id` varchar(100) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `content` varchar(200) DEFAULT NULL,
  `image_key` varchar(200) DEFAULT NULL,
  `type` tinyint(2) DEFAULT NULL,
  `sale_number` int(10) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `shop_id` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_package` */

/*Table structure for table `t_school_things` */

DROP TABLE IF EXISTS `t_school_things`;

CREATE TABLE `t_school_things` (
  `id` varchar(200) DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  `content` varchar(2000) DEFAULT NULL,
  `good_num` int(10) DEFAULT NULL,
  `click_num` int(10) DEFAULT NULL,
  `comment_id` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `is_hidden` tinyint(2) DEFAULT NULL COMMENT '是否隐藏身份',
  `create_time` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `t_school_things` */

/*Table structure for table `t_shop` */

DROP TABLE IF EXISTS `t_shop`;

CREATE TABLE `t_shop` (
  `id` varchar(100) NOT NULL,
  `category_id` varchar(100) DEFAULT NULL COMMENT '类别id',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  `info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '简介',
  `iphone` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态值',
  `end_time` decimal(10,2) DEFAULT NULL COMMENT '截止时间',
  `create_time` decimal(10,2) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_shop` */

/*Table structure for table `t_suggestion` */

DROP TABLE IF EXISTS `t_suggestion`;

CREATE TABLE `t_suggestion` (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '建议内容',
  `iphone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `t_suggestion` */

/*Table structure for table `t_task` */

DROP TABLE IF EXISTS `t_task`;

CREATE TABLE `t_task` (
  `id` varchar(100) DEFAULT NULL,
  `category_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类别id',
  `publish_user_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发布人id',
  `provider_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '服务者id',
  `content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '内容',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `work_time` datetime DEFAULT NULL COMMENT '服务开始时间',
  `secret_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '私密内容',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '取件码',
  `address` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地址'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_task` */

/*Table structure for table `t_used_goods` */

DROP TABLE IF EXISTS `t_used_goods`;

CREATE TABLE `t_used_goods` (
  `id` varchar(100) DEFAULT NULL,
  `category_id` tinyint(2) DEFAULT NULL COMMENT '类别id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '二手物品价格',
  `iphone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `tags` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标签',
  `image_ids` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态',
  `remark` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `deleted` tinyint(2) DEFAULT NULL COMMENT '逻辑删除标识',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `t_used_goods` */

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` varchar(100) DEFAULT NULL,
  `card_number` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '卡号',
  `type` int(10) DEFAULT NULL,
  `avatarUrl` varchar(100) DEFAULT NULL COMMENT '微信头像',
  `school_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '学校id',
  `true_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '真实姓名',
  `iphone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `start_date` date DEFAULT NULL COMMENT '起始日期',
  `image_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '认证图片id',
  `status` tinyint(2) DEFAULT NULL COMMENT '用户状态值0待支付 1支付成功，2审核通过 -1冻结',
  `blance` decimal(10,2) DEFAULT NULL COMMENT '余额',
  `fid` varchar(100) DEFAULT NULL COMMENT '推荐人id',
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ipenid',
  `end_time` datetime DEFAULT NULL COMMENT '到期时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_user` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
