package io.renren.modules.app.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IndexDao {

	List<HashMap<String, Object>> queryBanners(HashMap<String, Object> params);
	
	

}
