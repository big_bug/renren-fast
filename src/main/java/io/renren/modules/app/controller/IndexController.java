package io.renren.modules.app.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.R;
import io.renren.modules.app.annotation.Login;
import io.renren.modules.app.annotation.LoginUser;
import io.renren.modules.app.entity.UserEntity;
import io.renren.modules.app.service.IIndexService;
import io.renren.modules.app.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * APP登录授权
 * 
 * 
 */
@RestController
@RequestMapping("/app/index")
@Api("首页登录接口")
public class IndexController {
	
	
	@Autowired
    private IIndexService indexService;
	
	@Login
    @GetMapping("userInfo")
    @ApiOperation("首页信息")
    public R index(@LoginUser UserEntity user){
		HashMap<String,Object> params = new HashMap<> ();
		HashMap resObj =indexService.index(params);
		return R.ok().put("resObj", resObj);
		
    }
	

}
